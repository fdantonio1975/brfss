# Analisi del dataset BRFSS #

Oggetto di questo studio è l'analisi del dataset BRFSS realizzato dal CDC: Centers for Disease Control and Prevention.


Descrizione presa dal sito web:

"In 1984, the Centers for Disease Control and Prevention (CDC) initiated the state-based Behavioral Risk Factor Surveillance System (BRFSS)--a cross-sectional telephone survey that state health departments conduct monthly over landline telephones and cellular telephones with a standardized questionnaire and technical and methodologic assistance from CDC. BRFSS is used to collect prevalence data among adult U.S. residents regarding their risk behaviors and preventive health practices that can affect their health status."

Ci concentreremo sul dataset dell'anno **2015**.

[Dataset](https://www.cdc.gov/brfss/annual_data/2015/files/LLCP2015XPT.zip)

Il dataset consiste di **441456** osservazioni costituite da **331** variabili.

## Individuazione dei fenomeni di interesse

Scopo di questo studio sarà lo sviluppo di modelli predittivi per due fenomeni:

* Predizione di casi di **depressione**

Sas Variable Name: **ADDEPEV2**

Description: "(Ever told) you that you have a depressive disorder, including depression, major depression, dysthymia, or minor depression?"

* Predizione di casi di **pressione alta**

Sas Variable Name: **BPHIGH4**

Description: "Have you EVER been told by a doctor, nurse or other health professional that you have high blood pressure? (If "Yes" and respondent is female, ask "Was this only when you were pregnant?".)"

### Descrizione dell'ambiente di sviluppo processo implementato ###

* **Ambiente**: verranno utilizzate le seguenti tecnologie python, pandas, sklearn, matplotlib, mongodb, docker 
* **Analisi preliminare**: test chi2 e analisi correlazione 
* **Pre-Selezione delle variabili**: delle 331 totali verranno selezionate, a fini di test, un numero ristretto basato su common sense e test di correlazione
* **Scelta della variabile target**: come detto sopra ci concentreremo su ADDEPEV2 e BPHIGH4.
* **Preprocessamento**: essenzialmente codifica numerica per variabili binarie, OneHotEncoding per le variabili nominali non ordinali o encoding numerico per i nominali ordinali (quelli per cui Eccellente>Buono>Discreto etc.). 
* **Trattamento dei missing values**: scarto delle righe come prova iniziale; successivamente attribuzione con interpolazione lineare
* **Scaling delle variabili** con metodo StandardScaler. 
* **Sviluppo ed addestramento di modelli preditti mediante diversimetodi di classificazione**: LogisticRegression, Decision Trees, SGD, etc. 
* **Cross-validation** dei risultati rispetto ad accuracy. 
* **Feature selection** usando univariate selection e RFE (Recursive Feature Elimination) 
* **Model selection** finale


### Pre-selezione delle variabili ###

Le variabili pre-selezionate sono le seguenti:

* **SMOKE100**: "Have you smoked at least 100 cigarettes in your entire life?"
* **FLUSHOT6**: "During the past 12 months, have you had either a flu shot or a flu vaccine that was sprayed in your nose?" 
* **CVDINFR4**: "(Ever told) you had a heart attack, also called a myocardial infarction?"
* **INCOME2**: "annual household income from all sources"
* **RFHLTH**: "Adults with good or better health"
* **ADDEPEV2**: "(Ever told) you that you have a depressive disorder, including depression, major depression, dysthymia, or minor depression?"
* **CHECKUP1**: "About how long has it been since you last visited a doctor for a routine checkup?"
* **EXERANY2**: "During the past month, other than your regular job, did you participate in any physical activities or exercises such as running, calisthenics, golf, gardening, or walking for exercise? 
* **SMOKDAY2**: "Do you now smoke cigarettes every day, some days, or not at all?"
* **SEX**: "sex of respondent."
* **CVDSTRK3**: "(Ever told) you had a stroke" 
* **ASTHMA3**: "(Ever told) you had asthma?"
* **DECIDE**: "Because of a physical, mental, or emotional condition, do you have serious difficulty concentrating, remembering, or making decisions?
* **HAVARTH3**: "(Ever told) you have some form of arthritis, rheumatoid arthritis, gout, lupus, or fibromyalgia?"  
* **BPHIGH4**: "Have you EVER been told by a doctor, nurse or other health professional that you have high blood pressure?"  
* **RENTHOM1**: "Do you own or rent your home?" 
* **DIABETE3**: "(Ever told) you have diabetes"

## Significatività delle variabili scelte ##

Di seguito riportiamo la significatività delle variabili scelte rispetto alla variabile target **ADDEPEV2**:

|Nome|p-value|
|-|-|
|INCOME2|0.0|
|ASTHMA3|0.0|
|DECIDE|0.0|
|HAVARTH3|0.0|
|RFHLTH|1.73532385647e-188|
|RENTHOM1_RENT|1.03912668411e-166|
|SMOKE100|2.54560881345e-160|
|SEX|1.70401510505e-147|
|DIABETE3_1.0|1.3310842343e-90|
|CVDSTRK3|1.09480757412e-83|
|BPHIGH4|1.02886139741e-73|
|RENTHOM1_OWN|4.35828098155e-64|
|CVDINFR4|1.95534525446e-46|
|EXERANY2|2.45192737369e-38|
|SMOKDAY2|7.30158617726e-19|
|DIABETE3_0|3.08681656158e-16|
|RENTHOM1_OTHER|1.13618473123e-14|
|RENTHOM1_DK|6.60652779414e-10|
|FLUSHOT6|5.91259147386e-08|
|CHECKUP1|7.32477158701e-06|
|DIABETE3_DK|0.00158223577775|

Come si vede i p-value sono tutti molto piccoli il che indica che siamo sulla strada buona. 
Come si vede le variabili più significative in questo caso rappresentano il livello di reddito, la sofferenza d'asma, la difficoltà a prendere decisioni e, sorprendentemente i fenomeni di tipo artritico.
In merito a quest'ultimo fenomeno, a seguito di una veloce ricerca sul web, è emersa questa fonte che pare supportare quanto osservato:

> Rheumatoid arthritis and depression commonly occur together. Although this is known, people with rheumatoid arthritis often aren't screened for depression, so it may not be diagnosed or treated. Studies show that if depression occurring with rheumatoid arthritis isn't addressed, the treatment for rheumatoid arthritis itself can be less effective.

Confronta [Is depression a factor in rheumatoid arthritis?](http://www.mayoclinic.org/diseases-conditions/rheumatoid-arthritis/expert-answers/rheumatoid-arthritis-depression/faq-20119780)

Di seguito lo stesso test di significatività è riportato per la variabile **BPHIGH4**

|Nome|p-value|
|-|-|
|CVDINFR4|0.0|
|DIABETE3_1.0|0.0|
|HAVARTH3|0.0|
|CHECKUP1|0.0|
|CVDSTRK3|0.0|
|INCOME2|3.93825597369e-214|
|DIABETE3_0|1.05498247507e-211|
|RFHLTH|6.62948300812e-186|
|FLUSHOT6|3.02532603654e-150|
|DECIDE|9.57588660184e-136|
|SMOKE100|1.25499385287e-104|
|ADDEPEV2|2.97846370698e-96|
|EXERANY2|1.07739117487e-74|
|ASTHMA3|4.86479961482e-50|
|RENTHOM1_OTHER|1.64727957092e-45|
|RENTHOM1_RENT|2.89120229112e-43|
|RENTHOM1_OWN|5.34652907018e-31|
|SMOKDAY2|6.0739281969e-12|
|SEX|8.81337868987e-05|
|DIABETE3_DK|0.00128474986406|
|RENTHOM1_DK|0.0997584600137| 

In questo caso le variabili più significative sono legate a fenomeni cardiovascolari pregressi, diabete e anche all'avere fatto recentemente un CHECKUP generale.

## Correlazione con variabile ADDEPEV2 ##

A titolo di esempio riportiamo il coefficiente di correlazione fra le variabili e la variabile target.

|Nome|Pearson coefficient|
|-|-|
|INCOME2|-0.186062|
|SEX|-0.155583|
|SMOKDAY2|-0.111562|
|EXERANY2|-0.074879|
|CHECKUP1|-0.019264|
|FLUSHOT6|0.027397|
|CVDINFR4|0.034662|
|BPHIGH4|0.056717|
|CVDSTRK3|0.060668|
|ASTHMA3|0.154195|
|HAVARTH3|0.170383|
|DECIDE|0.375438|

I segni positivi ci danno la direzione della correlazione dei fenomeni. Ad esempio a livelli di reddito maggiori corrisponde maggiore propensione alla depressione. Le donne (ricordando che la codifica della variabile è 1 per gli uomini e 0 per le donne) sono più a rischio di casi depressivi. Invece la facoltà di prendere decisioni e la presenza di disturbi artritici sono correlati in senso positivo alla depressione.

## Preprocessamento del dataset ##

Il dataset in formato sas XPT è stato letto con pandas.read_sas utilizzando il flag iterator a True che permette lo streaming di dati (senza caricare quindi il dataset in memoria) e riversato sotto forma di oggetti json-like all'interno di un'istanza di db MongoDB (in container docker descritto all'interno del file docker-compose.yml).
Tale architettura permette di fare dei rapidi campionamenti dei dati mediante le funzionalità "aggregate" e "sample" offerte da MongoDB. I campioni sono stati usati per validare il processo nella sua interezza prima di effettuare lo sviluppo di un modello predittivo su campioni molto grandi dei dati (o su tutto il dataset).

Le variabili categoriche binarie sono state rappresentati mediante numeri 0 e 1. Mentre per le variabili non binarie non ordinali si è adottata la tecnica dello OneHotEncoding (essenzialmente si crea una variabile per ogni valore della variabile valorizzato ad uno quando la variabile assume quel valore).
Per le variabili categoriche ordinali si è adottata una codifica numerica crescente (lineare).

### Missing values ###

Inizialmente si è testato il processo scartando tutte le osservazioni che contenessero almeno un valore indefinito (null o NaN) per una delle variabile. La presenza di valori indefiniti in almeno una variabile è però molto diffusa e questo quindi avrebbe ridotto molto il dataset a disposizione per l'apprendimento successivo.
Pertanto si è optato per una interpolazione lineare (column-wise) per andare a riempire i valori mancanti.
Successivamente i valori sono stati normalizzati a media 0 e varianza 1 (anche se tale forma non è richiesta per tutti gli algoritmi nella fase di predizione).

## Sviluppo di un modello predittivo per ADDEPEV2 (depressione) ##

Di seguito i risultati ottenuti su un campione di 300k osservazioni.[^1]
[^1]: Per ragioni di tempo non è stato fatto un campione stratificato rispetto alla variabile target ma è ragionevalmente semplice da implementare.

I metodi utilizzati sono i seguenti:

* **Logistic Regression** (LR)
* **Logistic Regression CV** (LRCV): 

>This class implements logistic regression using liblinear, newton-cg, sag of lbfgs optimizer. The newton-cg, sag and lbfgs solvers support only L2 regularization with primal formulation. The liblinear solver supports both L1 and L2 regularization, with a dual formulation only for the L2 penalty.

* **Decision Tree**
* **Passive Aggressive Classifier (PA)**: margin-based, confronta [crammer06a.pdf](http://jmlr.csail.mit.edu/papers/volume7/crammer06a/crammer06a.pdf)
* **AdaBoost** con Descision Tree come classificatore base su cui costruire gli ensenble.
* **SGD**: SVM con discesa di gradiente stocastica

Di seguito riportiamo i risultati ottenuti con cross validation (20 iterazioni e percentuali 80%-20% per training e test set).
La baseline, trattandosi di target binario, è stata ottenuta come media della variabile target e rappresenta la probabilità di fare predizioni corrette utilizzando sempre il valore della variabile target più probabile.

|Metodo|Accuracy|
|-|-|
|Ada+Tree|0.802437858509|
|LRCV|0.80235818993|
|LR|0.800573613767|
|SGD|0.77279318037|
|Decision Tree|0.756668260038|
|baseline|0.755935498263|
|PA|0.718849585723|

Siamo in presenza di un fenomeno di "skewed classes" essendo una classe molto più probabile dell'altra.
L'incremento massimo rispetto alla baseline si attesta intorno al 5%.


## Sviluppo di un modello predittivo per BPHIGH4 (pressione alta) ##

|Metodo|Accuracy|
|-|-|
|LRCV|0.680129880415|
|Ada+Tree|0.678799398115|
|LR|0.677445157203|
|SGD|0.649362477231|
|Decision Tree|0.630228874634|
|PA|0.589506612814|
|baseline|0.536406113883|

In questo caso le classi sono più bilianciate (come si vede dalla "baseline").
L'incremento massimo rispetto alla baseline si attesta intorno al 15%.

## Feature selection ##
Sono stati applicati dei metodi per la riduzione del numero delle feature (variabili):

* **Univariate selection**: mediante test chi2. Questo metodo ha lo svantaggio di dover decidere il numero di feature che si vogliono mantenere ed è indipendente dal metodo di classificazione usato e molto veloce

* **RFE**:

> Given an external estimator that assigns weights to features (e.g., the coefficients of a linear model), the goal of recursive feature elimination (RFE) is to select features by recursively considering smaller and smaller sets of features. First, the estimator is trained on the initial set of features and weights are assigned to each one of them. Then, features whose absolute weights are the smallest are pruned from the current set features. That procedure is recursively repeated on the pruned set until the desired number of features to select is eventually reached.

Anche in questo caso il numero di feature va scelto a priori ma il processo di selezione è guidato da un classificatore base (generalmente SVM o SVC). Il processo richiede numerosi riaddestramenti ed è pertanto più lento del metodo precedente.

* **RFECV**: 

>Feature ranking with recursive feature elimination and cross-validated selection of the best number of features. 

Generalizza le operazioni del metodo precedente in modo da trovare il numero (sub)ottimale di feature. E' un metodo computazionalmente intensivo e soffre di una certa instabilità.

L'applicazione di tali metodi non ha portato risultati rilevanti dal punto di vista della performance dei modelli (accuracy) che è sempre rimasta uguale o poco peggiore.

# Conclusioni #
 
 E' stato realizzato uno studio di una pipeline complessa per l'analisi e lo sviluppo di modelli predittivi sul dataset BRFSS orientata al riconoscimento di casi di depressione e pressione sangugigna alta. I risultati possono considerarsi soddisfacenti con buoni incrementi rispetto alle baseline.
I modelli migliori sono risultati essere LogisticRegression (con variante CV) e AdaBoost+DecisionTree.
 Per sviluppi successivi del modello potrebbero essere adottate le seguenti strategie:
 
 * Aggiunta di nuove variabili
 * Analisi dei dataset anche relativi ad anni precedenti il 2015
 * Differente codifica/discretizzazione delle variabili
 * Utilizzo di algoritmi che possano cercare di catturare anche relazioni altamente non lineari fra i dati (p.e. Reti Neurali o Deep Learning)
 

### Bonus section ###

Il software sviluppato è disponibile su repository BitBucket pubblico: [https://bitbucket.org/fdantonio1975/brfss](https://bitbucket.org/fdantonio1975/brfss)

All'interno del software è presente anche un servizio RESTful (nella classe DataServices.py) che permette di ottenere campioni random dei dati preprocessati mediante chiamate HTTP.

In tale modo è possibile quindi offrire le possibilità di preprocessamento senza reimplementarle all'interno di altri tool/linguaggi come ad esempio R, SAS, etc.

>library(rjson)
url <- 'http://localhost:5000/sample/10000'
document <- fromJSON(file=url, method='C')


 







