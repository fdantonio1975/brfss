from flask.app import Flask
from brfss.config import AppConfig
from brfss.utils.DSUtils import random_sample
from brfss.transformations.DataTransformations import transfs,\
    FeaturesTransformation, Preprocess
from flask.wrappers import Response
import json
app=Flask("")


def tojson(f):
    def fun(*args,**kwargs):
        result=f(*args,**kwargs)
        status=200
        if isinstance(result, Response):
            return result
        dump= json.dumps(result,default=lambda o: o.__dict__)
            
        return Response(dump, status=status, mimetype='application/json')
    fun.__name__=f.__name__
    fun.__doc__=f.__doc__
    return fun

@app.route("/sample/<int:n>")
@tojson
def sample(n):
    df=random_sample(AppConfig.db, n,transfs.keys())
    pre=Preprocess(FeaturesTransformation(transfs))
    df=pre(df)
    return [x[1].to_dict() for x in df.iterrows()]

app.run(debug=True,threaded=True)