import numpy as np
import matplotlib.pyplot as plt

def doplot():


    dep1 = (20, 35)
    
    dep2 = (25, 32)
    
    fig, ax = plt.subplots()
    
    index = np.arange(len(dep1))
    bar_width = 0.35
    
    opacity = 0.4
    
    rects1 = plt.bar(index, dep1, bar_width,
                     alpha=opacity,
                     color='b',
                     label='Men')
    
    rects2 = plt.bar(index + bar_width, dep2, bar_width,
                     alpha=opacity,
                     color='r',
                     label='Women')
    
    plt.xlabel('Group')
    plt.ylabel('Perc')
    plt.title('Scores by group and gender')
    plt.xticks(index + bar_width, ('Depression',"Not Dep"))
    plt.legend()
    
    plt.tight_layout()
    plt.show()
doplot()