import pandas
from brfss.config import AppConfig
from brfss.utils.DSUtils import random_sample
from brfss.transformations.DataTransformations import transfs,\
    FeaturesTransformation, Preprocess, encode, standarscale
from sklearn.preprocessing.imputation import Imputer
from numpy import NaN
import numpy as np
from sklearn.linear_model.logistic import LogisticRegression
from brfss.apps.PredictDepression import MethodWrapper, approaches_score
from sklearn.feature_selection.univariate_selection import chi2, SelectKBest
from sklearn.preprocessing.data import MinMaxScaler
from scipy.stats.stats import pearsonr
import numpy
import matplotlib.pyplot as plt
from matplotlib.pyplot import savefig

n=100000
df=random_sample(AppConfig.db, n,transfs.keys())
target="ADDEPEV2"

pre=Preprocess(FeaturesTransformation(transfs))
df=pre(df)
#df=df[["SEX","HAVARTH3",target]]
df=df.dropna()

X,y=encode(df, target)
temp=df.corr()[target].copy()
temp.sort()
print(temp)


pandas.crosstab(df.HAVARTH3,df[target].astype(bool)).apply(lambda r: r/r.sum(), axis=1).plot(kind="bar")
savefig("/home/fulvio/workspaceluna2/BRFSS/images/fig1.png")
plt.show()
AppConfig.client.close()