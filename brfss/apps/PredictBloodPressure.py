from brfss.config import AppConfig
import numpy as np
from brfss.utils.DSUtils import random_sample
from brfss.transformations.DataTransformations import transfs,\
    FeaturesTransformation, missing_values_drop, Preprocess, encode,\
    standarscale
import pandas as pd
from sklearn.preprocessing.data import StandardScaler
from sklearn.linear_model.logistic import LogisticRegression,\
    LogisticRegressionCV
from sklearn.model_selection._split import ShuffleSplit, StratifiedKFold
from sklearn.model_selection._validation import cross_val_score
from sklearn.neighbors.classification import KNeighborsClassifier
from sklearn.neural_network.multilayer_perceptron import MLPClassifier
from sklearn.ensemble.weight_boosting import AdaBoostClassifier
from sklearn.tree.tree import DecisionTreeClassifier
from sklearn.feature_selection.univariate_selection import chi2, SelectKBest
from sklearn.feature_selection.rfe import RFECV
from sklearn.svm.classes import SVC
from sklearn.preprocessing.imputation import Imputer
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model.passive_aggressive import PassiveAggressiveClassifier
from sklearn.linear_model.stochastic_gradient import SGDClassifier

pd.set_option("mode.use_inf_as_null",True)

def approaches_score(methods,X,y,selection=None):
    mm=y.mean()
    ret={}
    ret['baseline']=max(mm,1.0-mm)
    if selection:
        X=selection(X,y)
    for method in methods:
        print(method.name)
        classifier=method()
        cv = ShuffleSplit(n_splits=10, test_size=0.2)
        score=cross_val_score(classifier,X,y,cv=cv)
        ret[method.name]=score.mean()  
    return ret 

class MethodWrapper:
    def __init__(self,method,name):
        self.method = method
        self.name=name
    def __call__(self):
        return self.method()

if __name__=="__main__":
    df=random_sample(AppConfig.db, 300000,transfs.keys())
    target="BPHIGH4"
    
    pre=Preprocess(FeaturesTransformation(transfs),missing_values_drop)
    df=pre(df)
    df=df[df[target].notnull()]
    
    X,y=encode(df, target)
    #X=X.interpolate()
    #X=X.fillna(value=0)

    X=standarscale(X)
    ##lr=MLPClassifier(max_iter=10000)
    #cv = ShuffleSplit(n_splits=10, test_size=0.2)
    #score=cross_val_score(lr,X,y,cv=cv)   
    #print(score.mean())
    #mm=y.mean()
    #print(max(mm,1.0-mm))
    methods=[]
    #methods.append(MethodWrapper(MultinomialNB,"NB"))
    methods.append(MethodWrapper(LogisticRegression,"LR"))
    methods.append(MethodWrapper(PassiveAggressiveClassifier,"PA"))
    methods.append(MethodWrapper(SGDClassifier,"SGD"))
    #methods.append(MethodWrapper(LogisticRegressionCV,"LRCV"))
    #methods.append(MethodWrapper(lambda:SVC(kernel="linear"),"SVC"))
    #methods.append(MethodWrapper(lambda:DecisionTreeClassifier(),"Decision Tree"))
    #methods.append(MethodWrapper(lambda:AdaBoostClassifier(DecisionTreeClassifier(max_depth=2)),"Ada+Tree"))
    #methods.append(MethodWrapper(lambda:AdaBoostClassifier(LogisticRegression()),"Ada+LR"))
    #methods.append(MethodWrapper(lambda:AdaBoostClassifier(LogisticRegressionCV()),"Ada+LRCV"))
    scores=approaches_score(methods, X.values, y)
    for k,v in sorted(scores.items(),lambda x,y:-cmp(x[1],y[1])):
        print(k,v)

    svc = SVC(kernel="linear")
    rfecv = RFECV(estimator=svc, step=1, cv=StratifiedKFold(2),scoring='accuracy')
    
    scores=approaches_score(methods, X, y,rfecv.fit_transform)
    print("Optimal number of features : %d" % rfecv.n_features_)
    print(X.columns[rfecv.support_])
    for k,v in sorted(scores.items(),lambda x,y:-cmp(x[1],y[1])):
        print(k,v)
        
        