import pandas
from brfss.config import AppConfig
from brfss.utils.DSUtils import random_sample
from brfss.transformations.DataTransformations import transfs,\
    FeaturesTransformation, Preprocess, encode, standarscale
from sklearn.preprocessing.imputation import Imputer
from numpy import NaN
import numpy as np
from sklearn.linear_model.logistic import LogisticRegression
from brfss.apps.PredictDepression import MethodWrapper, approaches_score
from sklearn.feature_selection.univariate_selection import chi2, SelectKBest
from sklearn.preprocessing.data import MinMaxScaler
from scipy.stats.stats import pearsonr
import numpy

df=random_sample(AppConfig.db, 100000,transfs.keys())
target="BPHIGH4"

pre=Preprocess(FeaturesTransformation(transfs))
df=pre(df)
df=df[df[target].notnull()]
X,y=encode(df, target)
X=X.interpolate()
X=X.fillna(value=0)


d={k:v  for (k,v) in zip(X.columns,chi2(X,y)[1])}
for k,v in sorted(d.items(),lambda x,y:cmp(x[1],y[1])):
    print("|%s|%s|"%(k,str(v)))
