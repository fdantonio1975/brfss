import pandas as pd

def random_sample(db,n,columns=None):
    if columns:
        columns=dict([(x,1) for x in columns])
        df=pd.DataFrame(list(db.data.aggregate([{"$sample":{"size":n}},{"$project":columns}],allowDiskUse=True)))
    else:
        df=pd.DataFrame(list(db.data.aggregate([{"$sample":{"size":n}}],allowDiskUse=True)))
    return df
