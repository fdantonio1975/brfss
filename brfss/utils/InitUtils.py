import pandas as pd
import sys
from brfss.config import AppConfig
import random

def initdb(db,xptpath,chunksize=30000):
    itr=pd.read_sas(xptpath,chunksize=chunksize)
    for el in itr:
        db.data.insert_many([r[1].to_dict() for r in el.iterrows()])
        sys.stdout.write(".")

if __name__=="__main__":
    AppConfig.db.data.remove({})
    
    initdb(AppConfig.db, AppConfig.xptpath)
