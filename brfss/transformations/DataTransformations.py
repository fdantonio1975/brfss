from numpy import NaN
import pandas as pd
from brfss.utils.DSUtils import random_sample
from brfss.config import AppConfig
import pandas
from sklearn.linear_model.logistic import LogisticRegression
from sklearn.preprocessing.data import StandardScaler
from sklearn.model_selection._split import ShuffleSplit
from sklearn.model_selection._validation import cross_val_score

class MapTransfom(dict):
    def __init__(self,d=None,defvalue="DK"):
        if d:
            self.update(d)
        self.defvalue = defvalue
    def __call__(self,x):
        if x is NaN:
            return NaN
        return self.get(x,self.defvalue)


class FeaturesTransformation:
    def __init__(self,transfs):
        self.transfs = transfs
    def __call__(self,df):
        df=df[self.transfs.keys()]
        for k,fun in self.transfs.items():
            df[k]=df[k].apply(fun)
        return df

def missing_values_drop(df):
    return df.dropna()

class Preprocess:
    def __init__(self,*transfs):
        self.transfs = transfs
    def __call__(self,object):
        for f in self.transfs:
            object=f(object)
        return object
def encode(df,target):
    cols=[k for k in df.columns if k!=target]
    X,y=pd.get_dummies(df[cols]),df[target]
    return X,y

def standarscale(X):
    scaler=StandardScaler()
    X[X.columns.values]=scaler.fit_transform(X)
    return X

transfs={}


transfs['CHECKUP1']=MapTransfom({1:1,2:2,3:3,4:4,8:5},NaN)
transfs['BPHIGH4']=MapTransfom({1:1,2:1,3:0},NaN)
#transfs['TOLDHI2']=MapTransfom({1:1,2:0},NaN)
transfs['CVDINFR4']=MapTransfom({1:1,2:0},NaN)
transfs['CVDSTRK3']=MapTransfom({1:1,2:0},NaN)
transfs['ASTHMA3']=MapTransfom({1:1,2:0},NaN)
transfs['HAVARTH3']=MapTransfom({1:1,2:0},NaN)
#transfs['ADDEPEV2']=MapTransfom({1:1,2:0},NaN)
transfs['ADDEPEV2']=MapTransfom({1:1,2:0},NaN) #when target
transfs['DIABETE3']=MapTransfom({1:1.0,2:1.0,3:0,4:0})
transfs['SEX']=MapTransfom({1:1,2:0},NaN)
transfs['RENTHOM1']=MapTransfom({1:"OWN",2:"RENT",3:"OTHER"})
#transfs['INCOME2']=MapTransfom({1:"<20k",2:"<20k",3:"<20k",4:">20k",5:">20k",6:">20k",7:">20k",8:">20k"})
transfs['INCOME2']=MapTransfom({1:1,2:2,3:3,4:4,5:5,6:6,7:7,8:8},NaN)
transfs['DECIDE']=MapTransfom({1:1,2:0},NaN)
transfs['SMOKE100']=MapTransfom({1:1,2:0},NaN)
transfs['SMOKDAY2']=MapTransfom({1:1,2:2,3:3},NaN)
transfs['EXERANY2']=MapTransfom({1:1,2:0},NaN)
transfs['FLUSHOT6']=MapTransfom({1:1,2:0},NaN)
transfs['_RFHLTH']=MapTransfom({1:1,2:0},NaN) #when target
#transfs['_RFHLTH']=MapTransfom({1:"Good",2:"NotGood"})
#transfs['LSATISFY']=MapTransfom({1:1,2:1,3:0,4:0},NaN)

if __name__=="__main__":
    for k in transfs:
        print("* "+k)
    
        